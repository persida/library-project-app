import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { Author } from '../../author/author.model';
import { AuthorService } from '../../author/author.service';
import { DataService } from '../../data.service';
import { Book } from '../book.model';
import { exhaustMap, tap } from 'rxjs/operators'

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.scss']
})
export class BookDetailComponent implements OnInit {
  id: number | undefined;
  book!: Book;
  author!: Author;

  constructor(private route: ActivatedRoute, 
              private dataService: DataService,
              private router: Router,
              private authorService: AuthorService) { }

  ngOnInit(): void {
    const rawBookId = this.route.snapshot.paramMap.get('id');

    if(rawBookId) {
      this.id = Number(rawBookId);
      this.dataService.fetchBookById(this.id)
      .pipe(
        tap(book => {
        this.book = book;
        console.log(this.book);
        }),
        exhaustMap((book) => this.authorService.fetchAuthorByName(book.authorName)))
      .subscribe(author => {
        this.author = author;
        console.log(this.author);
      });
    }

  }

}


