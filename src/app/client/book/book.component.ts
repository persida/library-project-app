import { Component, OnDestroy, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { TabDirective } from 'ngx-bootstrap/tabs';
import { Book } from './book.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit, OnDestroy {

  books: Book[]|undefined;
  bookSubcription!: Subscription;

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.books = this.dataService.getBooks();
    this.bookSubcription = this.dataService.booksChanged.subscribe({
      next: (books: Book[]) => {
        this.books = books;
      }
    })
  }

  ngOnDestroy(): void {
    this.bookSubcription.unsubscribe();
  }


  onSelect(data: TabDirective): void {
    this.dataService.format = data.id!;
    this.books = this.dataService.getBooks();
  }

}
