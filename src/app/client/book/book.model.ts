export class Book {
    public id: number;
    public title: string;
    public description: string;
    public addCost: number;
    public format: string;
    public category: string;
    public numPages: number;
    public duration: number;
    public imagePath: string;
    public authorName: string;

    constructor(id: number, title: string, description: string, addCost: number, format: string, category: string, numPages: number,
                duration: number, imagePath: string, authorName: string){
                    this.id = id;
                    this.title = title;
                    this.description = description;
                    this.addCost = addCost;
                    this.format = format;
                    this.category = category;
                    this.numPages = numPages;
                    this.duration = duration;
                    this.imagePath = imagePath;
                    this.authorName = authorName;
                }



}