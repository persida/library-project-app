import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from '../book.model';


@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.scss']
})
export class BookCardComponent implements OnInit {
  @Input() books: Book[]|undefined;

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
  }

  showBookDetails(book: Book) {
     this.router.navigate([book.id], {relativeTo: this.route});
  }

}
