import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookDetailComponent } from './book/book-detail/book-detail.component';
import { BookComponent } from './book/book.component';
import { ClientComponent } from './client.component';

const routes: Routes = [
  {path: '', component: ClientComponent, children: [
      {path: 'books', children :[
       {path: ':id',  component: BookDetailComponent},
       {path: '', pathMatch: 'full', component: BookComponent} 
        ]
      },
      {path: '**', redirectTo: 'books'}
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
