import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookComponent } from './book/book.component';
import { ClientRoutingModule } from './client-routing.module';
import { ClientComponent } from './client.component';
import { SharedModule } from '../shared/shared.module';
import { BookCardComponent } from './book/book-card/book-card.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BookDetailComponent } from './book/book-detail/book-detail.component';



@NgModule({
  declarations: [
    BookComponent,
    ClientComponent,
    BookCardComponent,
    BookDetailComponent
  ],
  imports: [
    ClientRoutingModule,
    SharedModule,
    TabsModule.forRoot()
  ]
})
export class ClientModule { }
