import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Author } from './author.model';

const API_ENDPOINT = 'authors';
const HEADERS = new HttpHeaders({
  Accept: 'application/json'
});

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  constructor(private http: HttpClient) { }

  fetchAuthorByName(name: string): Observable<Author> {

    const params = new HttpParams().append('author', name);

    return this.http.get<Author>(`${environment.urlAddress}/${API_ENDPOINT}`, {headers: HEADERS, params: params});

  }

}
