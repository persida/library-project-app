export class Author {
    constructor(
        public id: number,
        public name: string,
        public dateOfBirth: Date,
        public shortBiography: string
    ) {}
}