import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Page } from '../shared/page.model';
import { Book } from './book/book.model';

const API_ENDPOINT = 'books';
const HEADERS = new HttpHeaders({
  Accept: 'application/json'
});

@Injectable({
  providedIn: 'root'
})
export class DataService {
  format: string = 'book';
  books: Book[]|undefined;
  booksChanged = new Subject<Book[]>();
  httpParams: any;
  book: Book|undefined;

  constructor(private http: HttpClient) { }

  getBooks(searchPhrase? : string){
    this.fetchBooks(searchPhrase);
    return this.books;
  }

  public fetchBookById(id: number): Observable<Book> {

    return this.http.get<Book>(`${environment.urlAddress}/${API_ENDPOINT}/${id}`);
  }

  private fetchBooks(searchPhrase? : string){
    
    if(searchPhrase){
      this.httpParams = new HttpParams()
      .append('format', this.format)
      .append('title', searchPhrase)
      .append('author', searchPhrase)
      .append('category', searchPhrase)

    }else {
      this.httpParams = new HttpParams()
    .append('format', this.format);
    }

    this.http.get<Page<Book>>(`${environment.urlAddress}/${API_ENDPOINT}`, {headers: HEADERS, params: this.httpParams})
    .subscribe(books => {
      this.books = books.content;
      this.booksChanged?.next(this.books);
    });
  }
  
}
