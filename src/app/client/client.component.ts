import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-client',
  template: '<app-authenticated-layout></app-authenticated-layout>'
})
export class ClientComponent implements OnInit {

  constructor(private titleService: Title) { }

  ngOnInit(): void {
    this.titleService.setTitle('Search | E-Library');
  }

}
