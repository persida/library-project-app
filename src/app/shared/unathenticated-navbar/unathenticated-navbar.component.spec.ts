import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnathenticatedNavbarComponent } from './unathenticated-navbar.component';

describe('UnathenticatedNavbarComponent', () => {
  let component: UnathenticatedNavbarComponent;
  let fixture: ComponentFixture<UnathenticatedNavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnathenticatedNavbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnathenticatedNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
