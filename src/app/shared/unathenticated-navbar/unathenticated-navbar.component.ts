import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { LoginComponent } from 'src/app/login/login.component';

@Component({
  selector: 'app-unathenticated-navbar',
  templateUrl: './unathenticated-navbar.component.html',
  styleUrls: ['./unathenticated-navbar.component.scss']
})
export class UnathenticatedNavbarComponent implements OnInit {

  constructor(private modalService: BsModalService) { }

  ngOnInit(): void {
  }

  onLogin(){
    this.modalService.show(LoginComponent);
  }

}
