import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticatedLayoutComponent } from './authenticated-layout/authenticated-layout.component';
import { PublicLayoutComponent } from './public-layout/public-layout.component';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UnathenticatedNavbarComponent } from './unathenticated-navbar/unathenticated-navbar.component';
import { FooterComponent } from './footer/footer.component';
import { UnauthenticatedLayoutComponent } from './unauthenticated-layout/unauthenticated-layout.component';
import { HttpClientModule } from '@angular/common/http';




@NgModule({
  declarations: [
    AuthenticatedLayoutComponent,
    PublicLayoutComponent,
    NavbarComponent,
    UnathenticatedNavbarComponent,
    FooterComponent,
    UnauthenticatedLayoutComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  exports: [
    AuthenticatedLayoutComponent,
    PublicLayoutComponent,
    NavbarComponent,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    UnathenticatedNavbarComponent,
    FooterComponent,
    HttpClientModule
  ]
})
export class SharedModule { }
