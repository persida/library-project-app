export class Page<T>{
    constructor(
        public content: T[],
        public totalNumbeOfItems: number,
        public totalElements: number
    ){}
}