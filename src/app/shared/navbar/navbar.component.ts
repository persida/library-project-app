import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataService } from 'src/app/client/data.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {

  searchText: string|undefined

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
  }

  public logout():void {

  }

  onSubmit(form: NgForm){
    const searchText = form.value.searchText;
    this.dataService.getBooks(searchText);
    console.log(searchText);
    form.reset();
  }

}
