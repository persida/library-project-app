import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { UnauthenticatedLayoutComponent } from './shared/unauthenticated-layout/unauthenticated-layout.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'clients', loadChildren: () => import('./client/client.module').then(m => m.ClientModule)},
  {path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)},
  {path: '', pathMatch: 'full', component: UnauthenticatedLayoutComponent},
  {path: '**', redirectTo: 'clients'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: false})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
