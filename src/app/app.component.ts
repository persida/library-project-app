import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { setTheme } from 'ngx-bootstrap/utils';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  constructor(private titleService: Title){
    setTheme('bs4'); 
    this.titleService.setTitle('E-Library');
    
  }
}
