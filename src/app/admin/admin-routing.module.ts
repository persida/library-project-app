import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { AdminComponent } from './admin.component';
import { BookComponent } from './book/book.component';

const routes: Routes = [
  {path: '', component: AdminComponent, children: [
    {path: 'books', children :[
     {path: '', component: BookComponent} 
      ]
    },
    {path: '**', redirectTo: 'books'}
]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
