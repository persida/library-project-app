import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm = new FormGroup({
    username: new FormControl(null, []),
    password: new FormControl(null, [Validators.minLength(8)])
  })


  constructor(private router: Router, private bsModalService: BsModalService) { }

  ngOnInit(): void {
  }

  public onSubmit():void {
    this.bsModalService.hide();
    if(this.loginForm.invalid){
      console.log('Form is invalid!');
      console.log(this.loginForm.errors);
      return;
    }  
    console.log('Log In');
    this.router.navigateByUrl('clients');
  }

  onClose() {
    this.bsModalService.hide();
  }

}
